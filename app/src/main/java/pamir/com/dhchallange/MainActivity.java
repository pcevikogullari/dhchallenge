package pamir.com.dhchallange;

import android.app.SearchManager;
import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import pamir.com.dhchallange.API.DHApiService;
import pamir.com.dhchallange.API.DHPhoto;
import pamir.com.dhchallange.API.DHRestClient;
import pamir.com.dhchallange.Adapters.DHPhotoGridAdapter;
import pamir.com.dhchallange.Helpers.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private GridView gridView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ProgressBar progressBar;
    private LinearLayout internetConnectionLayout;
    private TextView reloadButton;

    private ArrayList<DHPhoto> photos = new ArrayList<>();
    private DHPhotoGridAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //region Initialize Views
        gridView = (GridView) findViewById(R.id.grid);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeToRefresh);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        internetConnectionLayout = (LinearLayout) findViewById(R.id.internetConnectionLayout);
        reloadButton = (TextView) findViewById(R.id.reloadButton);
        //endregion

        //region Set Listeners
        adapter = new DHPhotoGridAdapter(this, photos);
        gridView.setAdapter(adapter);

        reloadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getPhotos();
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getPhotos();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
        //endregion

        getPhotos();

    }

    private void getPhotos() {

        if (Utils.isInternetAvailable(getApplicationContext())) {
            internetConnectionLayout.setVisibility(View.INVISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            gridView.setVisibility(View.VISIBLE);

            Call<ArrayList<DHPhoto>> call = DHRestClient.getApiService().getPhotos();
            call.enqueue(new Callback<ArrayList<DHPhoto>>() {
                @Override
                public void onResponse(Call<ArrayList<DHPhoto>> call, Response<ArrayList<DHPhoto>> response) {
                    adapter = new DHPhotoGridAdapter(MainActivity.this, response.body());
                    gridView.setAdapter(adapter);
                    progressBar.setVisibility(View.INVISIBLE);
                }

                @Override
                public void onFailure(Call<ArrayList<DHPhoto>> call, Throwable t) {
                    progressBar.setVisibility(View.INVISIBLE);
                    Toast.makeText(getApplicationContext(), getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                    t.printStackTrace();
                }
            });

        } else {
            progressBar.setVisibility(View.INVISIBLE);
            gridView.setVisibility(View.INVISIBLE);
            internetConnectionLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        MenuItem searchMenuItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchMenuItem.getActionView();

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(this);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        adapter.filter(newText);
        return true;
    }
}
