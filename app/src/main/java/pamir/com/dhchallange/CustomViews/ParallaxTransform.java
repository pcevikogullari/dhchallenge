package pamir.com.dhchallange.CustomViews;

import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;

import pamir.com.dhchallange.R;

public class ParallaxTransform implements ViewPager.PageTransformer {

  private float parallaxSpeed = 0.3f;

  @Override
  public void transformPage( View view, float v) {
    ImageView imageView = (ImageView)view.findViewById(R.id.detailPreview);

    if( imageView == null){return;}
    if(v <= -1 || v >= 1){ return;}

    imageView.setTranslationX(-v * parallaxSpeed * view.getWidth());
  }
}