package pamir.com.dhchallange.API;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Pamir on 23.06.2016.
 */

public class DHRestClient {
    public static final String BASE_URL = "http://jsonplaceholder.typicode.com";
    private static DHApiService apiService;

    public static DHApiService getApiService() {
        if (apiService == null) {

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            apiService = retrofit.create(DHApiService.class);

        }
        return apiService;
    }
}
