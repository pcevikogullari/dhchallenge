package pamir.com.dhchallange.API;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Pamir on 23.06.2016.
 */

public interface DHApiService {
    @GET("albums/1/photos")
    Call<ArrayList<DHPhoto>> getPhotos();
}
