package pamir.com.dhchallange.API;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Pamir on 23.06.2016.
 */

public class DHPhoto implements Parcelable{
    public int albumId;
    public int id;
    public String title;
    public String url;
    public String thumbnailUrl;

    protected DHPhoto(Parcel in) {
        albumId = in.readInt();
        id = in.readInt();
        title = in.readString();
        url = in.readString();
        thumbnailUrl = in.readString();
    }

    public static final Creator<DHPhoto> CREATOR = new Creator<DHPhoto>() {
        @Override
        public DHPhoto createFromParcel(Parcel in) {
            return new DHPhoto(in);
        }

        @Override
        public DHPhoto[] newArray(int size) {
            return new DHPhoto[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(albumId);
        parcel.writeInt(id);
        parcel.writeString(title);
        parcel.writeString(url);
        parcel.writeString(thumbnailUrl);
    }
}
