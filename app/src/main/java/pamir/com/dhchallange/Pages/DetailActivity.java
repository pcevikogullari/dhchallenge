package pamir.com.dhchallange.Pages;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;

import pamir.com.dhchallange.API.DHPhoto;
import pamir.com.dhchallange.Adapters.ViewPagerAdapter;
import pamir.com.dhchallange.CustomViews.ParallaxTransform;
import pamir.com.dhchallange.R;

public class DetailActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private ViewPagerAdapter adapter;
    private ArrayList<DHPhoto> photos;
    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        //region Handle Intent Data
        Bundle bundle = getIntent().getExtras();
        photos = bundle.getParcelableArrayList("photos");
        position = bundle.getInt("position");
        setCurrentPagePosition(position);
        //endregion

        //region Initialize Views
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        adapter = new ViewPagerAdapter(this, photos);
        viewPager.setAdapter(adapter);
        viewPager.setPageTransformer(false, new ParallaxTransform());
        viewPager.setCurrentItem(position);
        //endregion

        //region Set Listeners
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int i) {
                setCurrentPagePosition(i);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        //endregion
    }

    private void setCurrentPagePosition(int position) {
        setTitle((position+1) + "/" + photos.size());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }
}
