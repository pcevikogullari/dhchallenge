package pamir.com.dhchallange.Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import org.w3c.dom.Text;

import java.util.ArrayList;

import pamir.com.dhchallange.API.DHPhoto;
import pamir.com.dhchallange.R;

/**
 * Created by Pamir on 24.06.2016.
 */

public class ViewPagerAdapter extends PagerAdapter {

    private LayoutInflater inflater;
    private Activity activity;
    private ArrayList<DHPhoto> photos;

    public ViewPagerAdapter(Activity activity, ArrayList<DHPhoto> photos) {
        this.activity = activity;
        this.photos = photos;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.detail, container, false);

        ImageView detailPreview = (ImageView) view.findViewById(R.id.detailPreview);
        TextView title = (TextView) view.findViewById(R.id.title);
        final ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        DHPhoto photo = photos.get(position);

        Glide.with(activity).load(photo.url).listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                progressBar.setVisibility(View.INVISIBLE);
                return false;
            }
        }).into(detailPreview);

        title.setText(photo.title);

        container.addView(view);

        return view;
    }

    @Override
    public int getCount() {
        return photos.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((View) object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
