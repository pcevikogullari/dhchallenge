package pamir.com.dhchallange.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;
import java.util.HashMap;
import pamir.com.dhchallange.API.DHPhoto;
import pamir.com.dhchallange.CustomViews.SquareImageView;
import pamir.com.dhchallange.Pages.DetailActivity;
import pamir.com.dhchallange.R;

/**
 * Created by Pamir on 23.06.2016.
 */

public class DHPhotoGridAdapter extends BaseAdapter
{
    private Activity activity;
    private ArrayList<DHPhoto> photos;
    private ArrayList< DHPhoto > filteredPhotos;

    public DHPhotoGridAdapter( Activity activity, ArrayList< DHPhoto > photos )
    {
        this.activity = activity;
        this.photos = photos;
        this.filteredPhotos = new ArrayList<DHPhoto>();
        this.filteredPhotos.addAll(photos);
    }

    @Override
    public int getCount()
    {
        return photos.size();
    }

    @Override
    public Object getItem( int position )
    {
        return photos.get(position);
    }

    @Override
    public long getItemId( int position )
    {
        return position;
    }


    private static class ViewHolder {
        ImageView preview;
        ProgressBar progressBar;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent )
    {
        final ViewHolder viewHolder;

        if(convertView == null){
            LayoutInflater inflater = LayoutInflater.from(activity.getApplicationContext());
            convertView = inflater.inflate(R.layout.photo_grid_cell, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.preview = (SquareImageView) convertView.findViewById(R.id.preview);
            viewHolder.progressBar = (ProgressBar) convertView.findViewById(R.id.progressBar);

            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder)convertView.getTag();
        }

        final DHPhoto photo = photos.get(position);

        viewHolder.progressBar.setVisibility(View.VISIBLE); //reset previous state of progressbar
        if( photo != null){
            Glide.with(activity).load(photo.thumbnailUrl).listener(new RequestListener<String, GlideDrawable>() {
                @Override
                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                    return false;
                }

                @Override
                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                    viewHolder.progressBar.setVisibility(View.INVISIBLE);
                    return false;
                }
            }).into(viewHolder.preview);
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, DetailActivity.class);
                intent.putParcelableArrayListExtra("photos", photos);
                intent.putExtra("position", position);
                activity.startActivity(intent);
            }
        });

        return convertView;

    }

    public void filter( String s )
    {
        s = s.toLowerCase();
        photos.clear();

        for ( DHPhoto photo : filteredPhotos ) {
            if ( photo.title.toLowerCase().contains(s) ) {
                photos.add(photo);
            }
        }

        notifyDataSetChanged();
    }

}
